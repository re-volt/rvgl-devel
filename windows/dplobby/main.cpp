﻿////////////////////////////////////////////////////////////////////////////////
//
// RVGL (Generic) Copyright (c) The RV Team
//
// Desc: main.cpp
// DirectPlay Lobby helper module.
//
// 2016-01-12 (Huki)
// File inception.
//
////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <dplobby.h>
#include <stdio.h>

////////////////////////////////////////////////////////////////////////////////
// Prototypes:
////////////////////////////////////////////////////////////////////////////////

extern "C" __declspec(dllexport) bool GetLobbiedStatus(char *ip_address);

////////////////////////////////////////////////////////////////////////////////
// Globals:
////////////////////////////////////////////////////////////////////////////////

/// directplay GUIDs
const GUID CLSID_DirectPlayLobby = {0x2fe8f810, 0xb2a5, 0x11d0, 0xa7, 0x87, 0x0, 0x0, 0xf8, 0x3, 0xab, 0xfc};
const GUID IID_IDirectPlayLobby3A = {0x2db72491, 0x652c, 0x11d1, 0xa7, 0xa8, 0x0, 0x0, 0xf8, 0x3, 0xab, 0xfc};
const GUID IID_IDirectPlay4A = {0xab1c531, 0x4745, 0x11d1, 0xa7, 0xa1, 0x0, 0x0, 0xf8, 0x3, 0xab, 0xfc};
const GUID DPAID_INet = {0xc4a54da0, 0xe0af, 0x11cf, 0x9c, 0x4e, 0x0, 0xa0, 0xc9, 0x5, 0x42, 0x5e};

////////////////////////////////////////////////////////////////////////////////
// Procedures:
////////////////////////////////////////////////////////////////////////////////

///
/// check if we are lobbied
/// @param ip_address host's ip string or empty if host
/// @return whether we are lobbied or not
///

bool GetLobbiedStatus(char *ip_address)
{
  // init COM
  HRESULT r = CoInitialize(NULL);
  if (r != S_OK && r != S_FALSE) {
    return false;
  }

  // create lobby object
  IDirectPlayLobby3 *lobby;
  r = CoCreateInstance(CLSID_DirectPlayLobby, NULL, CLSCTX_INPROC_SERVER, IID_IDirectPlayLobby3A, (void**)&lobby);
  if (r != S_OK) {
    CoUninitialize();
    return false;
  }

  // launched by a lobby?
  DWORD size;
  r = lobby->GetConnectionSettings(0, NULL, &size);
  if (r == DPERR_NOTLOBBIED) {
    lobby->Release();
    CoUninitialize();
    return false;
  }

  // get connection settings
  DPLCONNECTION *connection = (DPLCONNECTION *)malloc(size);
  if (!connection) {
    lobby->Release();
    CoUninitialize();
    return false;
  }

  r = lobby->GetConnectionSettings(0, connection, &size);
  if (r != DP_OK) {
    free(connection);
    lobby->Release();
    CoUninitialize();
    return false;
  }

  // extract ip address
  sprintf(ip_address, "%s", "");
  if (connection->dwFlags != DPLCONNECTION_CREATESESSION) {
    DPADDRESS *addr = (DPADDRESS *)connection->lpAddress;
    while((char *)addr - (char *)connection->lpAddress < (signed long)connection->dwAddressSize) {
      if (addr->guidDataType == DPAID_INet) {
        strncpy(ip_address, (char *)(addr+1), addr->dwDataSize);
        break;
      } else {
        addr = (DPADDRESS *)((char *)(addr+1) + addr->dwDataSize);
      }
    }
  }

  // dummy connect  // #NOTE: Required by some lobby launchers like GameRanger.
  if (connection->dwFlags == DPLCONNECTION_CREATESESSION) {
    IDirectPlay4A *dp;
    r = lobby->ConnectEx(0, IID_IDirectPlay4A, (void**)&dp, NULL);
    if (r == DP_OK) {
      dp->Release();  // #NOTE: This calls WSA_Cleanup() which crashes on Wine when launched from GameRanger.
    }
  }

  free(connection);
  lobby->Release();
  CoUninitialize();
  return true;
}
