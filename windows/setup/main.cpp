////////////////////////////////////////////////////////////////////////////////
//
// RVGL (Generic) Copyright (c) The RV Team
//
// Desc: main.cpp
// Windows setup for RVGL.
//
// 2014-09-01 (Huki)
// File inception.
//
////////////////////////////////////////////////////////////////////////////////

#include <process.h>
#include <windows.h>
#include <shlobj.h>
#include <commctrl.h>
#include <sddl.h>
#include <aclapi.h>
#include <dplobby.h>
#include <stdio.h>
#include "resource.h"
#include "7zip/7z.h"
#include "7zip/7zAlloc.h"
#include "7zip/7zCrc.h"
#include "7zip/7zFile.h"
#include "7zip/CpuArch.h"

char g_szAppPath[MAX_PATH];
char g_szCurrentDir[MAX_PATH];
BOOL g_bInstalling = FALSE;
HANDLE g_hFontHandle = NULL;

void WaitForMessages(HWND hwndDlg, int nIter)
{
    MSG msg;
    int i = 0;

    do
    {
        while (PeekMessage(&msg, hwndDlg, 0, 0, PM_REMOVE))
        {
            if (msg.message != WM_QUIT)
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }

        if (nIter) Sleep(10);
        i++;
    }
    while (i < nIter);
}

#define kBufferSize (1 << 15)
#define kSignatureSearchLimit (1 << 22)
static Bool FindSignature(CSzFile *stream, UInt64 *resPos)
{
    Byte buf[kBufferSize];
    size_t numPrevBytes = 0;
    *resPos = 0;

    for (;;)
    {
        size_t numTests, pos;
        if (*resPos > kSignatureSearchLimit)
            return False;

        do
        {
            size_t processed = kBufferSize - numPrevBytes;
            if (File_Read(stream, buf + numPrevBytes, &processed) != 0)
                return False;
            if (processed == 0)
                return False;
            numPrevBytes += processed;
        }
        while (numPrevBytes <= k7zStartHeaderSize);

        numTests = numPrevBytes - k7zStartHeaderSize;
        for (pos = 0; pos < numTests; pos++)
        {
            for (; buf[pos] != '7' && pos < numTests; pos++);
            if (pos == numTests)
                break;
            if (memcmp(buf + pos, k7zSignature, k7zSignatureSize) == 0)
            {
                if (CrcCalc(buf + pos + 12, 20) == GetUi32(buf + pos + 8))
                {
                    *resPos += pos;
                    return True;
                }
            }
        }

        *resPos += numTests;
        numPrevBytes -= numTests;
        memmove(buf, buf + numTests, numPrevBytes);
    }
}

int DoInstall(HWND hwndDlg)
{
    CFileInStream archiveStream;
    CLookToRead lookStream;
    CSzArEx db;
    SRes res = SZ_OK;
    ISzAlloc allocImp;
    ISzAlloc allocTempImp;
    UInt16 *temp = NULL;
    char sfxPath[MAX_PATH];
    size_t tempSize = 0;

    CrcGenerateTable();

    allocImp.Alloc = SzAlloc;
    allocImp.Free = SzFree;

    allocTempImp.Alloc = SzAllocTemp;
    allocTempImp.Free = SzFreeTemp;

    FileInStream_CreateVTable(&archiveStream);
    LookToRead_CreateVTable(&lookStream, False);
#ifndef _DEBUG
    if (!GetModuleFileName(NULL, sfxPath, MAX_PATH))
        return SZ_ERROR_FAIL;
#else
    sprintf(sfxPath, "%s\\package.7z", g_szCurrentDir);
#endif
    if (InFile_Open(&archiveStream.file, sfxPath) != SZ_OK)
        return SZ_ERROR_FAIL;
    else
    {
        UInt64 pos = 0;
        if (!FindSignature(&archiveStream.file, &pos))
            res = SZ_ERROR_FAIL;
        else if (File_Seek(&archiveStream.file, (Int64 *)&pos, SZ_SEEK_SET) != 0)
            res = SZ_ERROR_FAIL;

        if (res != SZ_OK)
        {
            File_Close(&archiveStream.file);
            return res;
        }
    }

    lookStream.realStream = &archiveStream.s;
    LookToRead_Init(&lookStream);

    SzArEx_Init(&db);
    res = SzArEx_Open(&db, &lookStream.s, &allocImp, &allocTempImp);
    if (res == SZ_OK)
    {
        UInt32 i;

        UInt32 blockIndex = 0xFFFFFFFF;
        Byte *outBuffer = 0;
        size_t outBufferSize = 0;

        SendDlgItemMessage(hwndDlg, IDC_PROGRESS, PBM_SETRANGE, 0, MAKELPARAM(0, db.db.NumFiles));
        SendDlgItemMessage(hwndDlg, IDC_PROGRESS, PBM_SETSTEP, (WPARAM)1, 0);
        SendDlgItemMessage(hwndDlg, IDC_PROGRESS, PBM_SETPOS, 0, 0);

        for (i = 0; i < db.db.NumFiles; i++)
        {
            size_t offset = 0;
            size_t outSizeProcessed = 0;
            const CSzFileItem *f = db.db.Files + i;
            size_t len;

            len = SzArEx_GetFileNameUtf16(&db, i, NULL);
            if (len > tempSize)
            {
                SzFree(NULL, temp);
                tempSize = len;
                temp = (UInt16 *)SzAlloc(NULL, tempSize * sizeof(temp[0]));
                if (temp == 0)
                {
                    res = SZ_ERROR_MEM;
                    break;
                }
            }

            SzArEx_GetFileNameUtf16(&db, i, temp);

            if (!f->IsDir)
            {
                res = SzArEx_Extract(&db, &lookStream.s, i,
                    &blockIndex, &outBuffer, &outBufferSize,
                    &offset, &outSizeProcessed,
                    &allocImp, &allocTempImp);
                if (res != SZ_OK)
                    break;
            }

            SendDlgItemMessage(hwndDlg, IDC_PROGRESS, PBM_STEPIT, 0, 0);
            WaitForMessages(hwndDlg, 0);

            CSzFile outFile;
            size_t processedSize;
            size_t j;
            UInt16 *name = (UInt16 *)temp;
            const UInt16 *destPath = (const UInt16 *)name;
            for (j = 0; name[j] != 0; j++)
            {
                if (name[j] == '/')
                {
                    name[j] = 0;
                    CreateDirectoryW((LPCWSTR)name, NULL);
                    name[j] = CHAR_PATH_SEPARATOR;
                }
            }

            if (f->IsDir)
            {
                SetFileAttributesW((LPCWSTR)destPath, FILE_ATTRIBUTE_DIRECTORY);
                if (!CreateDirectoryW((LPCWSTR)destPath, NULL) && (GetLastError() != ERROR_ALREADY_EXISTS))
                {
                    res = SZ_ERROR_WRITE;
                    break;
                }
                continue;
            }
            else
            {
                SetFileAttributesW((LPCWSTR)destPath, 0); //FILE_ATTRIBUTE_NORMAL
                if (OutFile_OpenW(&outFile, (WCHAR*)destPath) != SZ_OK)
                {
                    res = SZ_ERROR_WRITE;
                    break;
                }
            }

            processedSize = outSizeProcessed;
            if ((File_Write(&outFile, outBuffer + offset, &processedSize) != SZ_OK) || (processedSize != outSizeProcessed))
            {
                res = SZ_ERROR_WRITE;
                break;
            }
            if (f->MTimeDefined)
            {
                FILETIME mTime;
                mTime.dwLowDateTime = f->MTime.Low;
                mTime.dwHighDateTime = f->MTime.High;
                SetFileTime(outFile.handle, NULL, NULL, &mTime);
            }
            if (File_Close(&outFile) != SZ_OK)
            {
                res = SZ_ERROR_WRITE;
                break;
            }
            if (f->AttribDefined)
                SetFileAttributesW((LPCWSTR)destPath, f->Attrib);
        }

        IAlloc_Free(&allocImp, outBuffer);
    }

    SzArEx_Free(&db, &allocImp);
    SzFree(NULL, temp);

    File_Close(&archiveStream.file);
    return res;
}

#define GUID_DP_RV      {0x6bb78285, 0x71df, 0x11d2, 0xb4, 0x6c, 0x0c, 0x78, 0x0c, 0xc1, 0x08, 0x40}
#define GUID_DP_IID     {0x2db72491, 0x652c, 0x11d1, 0xa7, 0xa8, 0x0,  0x0,  0xf8, 0x3,  0xab, 0xfc}
#define GUID_DP_CLSID   {0x2fe8f810, 0xb2a5, 0x11d0, 0xa7, 0x87, 0x0,  0x0,  0xf8, 0x3,  0xab, 0xfc}
BOOL RegisterLobby(void)
{
    HRESULT r;
    DPAPPLICATIONDESC app;
    LPDIRECTPLAYLOBBY3 Lobby;

    const GUID CLSID_DirectPlayLobby = GUID_DP_CLSID;
    const GUID IID_IDirectPlayLobby3A = GUID_DP_IID;
    const GUID guidApp = GUID_DP_RV;

// spawn process

    _spawnl(_P_WAIT, "rvgl.exe", "rvgl.exe", "-register", NULL);

// init COM

    r = CoInitialize(NULL);
    if (r != S_OK)
        return FALSE;

// create lobby object

    r = CoCreateInstance(CLSID_DirectPlayLobby, NULL, CLSCTX_INPROC_SERVER, IID_IDirectPlayLobby3A, (void**)&Lobby);
    if (r != DP_OK)
    {
        CoUninitialize();
        return FALSE;
    }

// register

    app.dwSize = sizeof(app);
    app.dwFlags = 0;
    app.lpszApplicationNameA = (LPSTR)"Re-Volt";
    app.guidApplication = guidApp;
    app.lpszFilenameA = (LPSTR)"rvgl.exe";
    app.lpszCommandLineA = (LPSTR)"";
    app.lpszPathA = g_szAppPath;
    app.lpszCurrentDirectoryA = g_szAppPath;
    app.lpszDescriptionA = NULL;
    app.lpszDescriptionW = NULL;

    r = Lobby->RegisterApplication(0, &app);
    Lobby->Release();
    CoUninitialize();

    if (r != DP_OK)
        return FALSE;

    return TRUE;
}

#define REG_INSTALL_KEY     "software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\revolt.exe"
#define REG_DIRECTPLAY_KEY  "software\\Microsoft\\DirectPlay\\Applications\\Re-Volt"
void SetRegistryPath(const char *pszSubKey)
{
    HKEY key;

    if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, pszSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &key, NULL) == ERROR_SUCCESS)
    {
        RegSetValueEx(key, "Path", 0, REG_SZ, (unsigned char*)g_szAppPath, MAX_PATH);
        RegCloseKey(key);
    }
}

void GetRegistryPath(const char *pszSubKey)
{
    HKEY key;
    char buf[MAX_PATH];

    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszSubKey, 0, KEY_READ, &key) == ERROR_SUCCESS)
    {
        DWORD dwSize = MAX_PATH;
        if (RegQueryValueEx(key, "Path", 0, NULL, (unsigned char*)buf, &dwSize) == ERROR_SUCCESS)
        {
            buf[dwSize - 1] = 0;
            strcpy(g_szAppPath, buf);
        }

        RegCloseKey(key);
    }
}

void CreateShortcut(void)
{
    char szDesktopPath[MAX_PATH];
    char szLinkFile[MAX_PATH];
    char szTarget[MAX_PATH];
    char szDesc[MAX_PATH];

    if (SHGetFolderPath(NULL, CSIDL_COMMON_DESKTOPDIRECTORY, NULL, 0, szDesktopPath) != S_OK)
        return;

    sprintf(szLinkFile, "%s\\RVGL.lnk", szDesktopPath);
    sprintf(szTarget, "%s\\rvgl.exe", g_szAppPath);
    sprintf(szDesc, "Re-Volt OpenGL Port");

    WIN32_FIND_DATA fd;

    HANDLE handle = FindFirstFile(szLinkFile, &fd);
    if (handle != INVALID_HANDLE_VALUE)
    {
        FindClose(handle);
        return;
    }

    HRESULT r;
    IShellLink *psl;
    IPersistFile *ppf;
    WCHAR wsz[MAX_PATH];

    r = CoInitialize(NULL);
    if (r != S_OK)
        return;

    r = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID*)&psl);
    if (r != S_OK)
    {
        CoUninitialize();
        return;
    }

    psl->SetPath(szTarget);
    psl->SetDescription(szDesc);
    psl->SetWorkingDirectory(g_szAppPath);

    r = psl->QueryInterface(IID_IPersistFile, (LPVOID*)&ppf);
    if (r != S_OK)
    {
        CoUninitialize();
        return;
    }

    if (!MultiByteToWideChar(CP_ACP, 0, szLinkFile, -1, wsz, MAX_PATH))
        return;

    r = ppf->Save(wsz, TRUE);

    ppf->Release();
    psl->Release();
    CoUninitialize();
}

void FinalizeInstall(HWND hwndDlg, int res)
{
    if (res != SZ_OK)
    {
        const char *cszText = "Installation package is corrupt!";
        if (res == SZ_ERROR_MEM)
            cszText = "Out of memory!";
        else if (res == SZ_ERROR_WRITE)
            cszText = "Access denied!";

        MessageBox(hwndDlg, cszText, "Error", MB_OK);
        MessageBox(hwndDlg, "Setup was not successful.", "Error", MB_OK);
        return;
    }

    DWORD dwSize, dwSdSize, dwDaclSize;
    PSECURITY_DESCRIPTOR lpSD = NULL;
    PISECURITY_DESCRIPTOR pSd = NULL;
    const char *szSD = "D:(A;OICI;GA;;;AU)";
    BOOL bFailure = FALSE;
    PACL pDacl = NULL;

// set file and folder ACLs

    dwSdSize = dwDaclSize = dwSize = 0;

    if (!ConvertStringSecurityDescriptorToSecurityDescriptor(
        szSD,
        SDDL_REVISION_1,
        &lpSD,
        NULL) ||

        (!MakeAbsoluteSD(lpSD, pSd, &dwSdSize,
        pDacl, &dwDaclSize,
        NULL, &dwSize,
        NULL, &dwSize,
        NULL, &dwSize) &&
        GetLastError() != ERROR_INSUFFICIENT_BUFFER) ||

        !(pSd = (PISECURITY_DESCRIPTOR)malloc(dwSdSize)) ||
        !(pDacl = (PACL)malloc(dwDaclSize)) ||

        !MakeAbsoluteSD(lpSD, pSd, &dwSdSize,
        pDacl, &dwDaclSize,
        NULL, &dwSize,
        NULL, &dwSize,
        NULL, &dwSize) ||

        (SetNamedSecurityInfo((LPSTR)"cache", SE_FILE_OBJECT,
        DACL_SECURITY_INFORMATION, NULL, NULL,
        pDacl, NULL) != ERROR_SUCCESS) ||

        (SetNamedSecurityInfo((LPSTR)"profiles", SE_FILE_OBJECT,
        DACL_SECURITY_INFORMATION, NULL, NULL,
        pDacl, NULL) != ERROR_SUCCESS) ||

        (SetNamedSecurityInfo((LPSTR)"replays", SE_FILE_OBJECT,
        DACL_SECURITY_INFORMATION, NULL, NULL,
        pDacl, NULL) != ERROR_SUCCESS) ||

        (SetNamedSecurityInfo((LPSTR)"times", SE_FILE_OBJECT,
        DACL_SECURITY_INFORMATION, NULL, NULL,
        pDacl, NULL) != ERROR_SUCCESS))
    {
#ifndef _DEBUG
        MessageBox(hwndDlg, "Failed to set file permissions!", "Error", MB_OK);
        bFailure = TRUE;
#endif
    }

    if (pSd) free(pSd);
    if (pDacl) free(pDacl);
    if (lpSD) LocalFree(lpSD);

// register for lobby

    if (!RegisterLobby())
    {
#if 0 //ndef _DEBUG
        MessageBox(hwndDlg, "Can't register for lobby support!", "Error", MB_OK);
        bFailure = TRUE;
#endif
    }

    SetRegistryPath(REG_INSTALL_KEY);
    SetRegistryPath(REG_DIRECTPLAY_KEY);

// create desktop shortcut

    if (IsDlgButtonChecked(hwndDlg, IDC_SHORTCUT) == BST_CHECKED)
        CreateShortcut();

    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);

    if (bFailure)
        MessageBox(hwndDlg, "Setup was not successful.", "Error", MB_OK);
    else
        MessageBox(hwndDlg, "Setup completed successfully.", "Message", MB_OK);

    return;
}

void SetAppPath(HWND hwndDlg)
{
    HWND hwndCtl = GetDlgItem(hwndDlg, IDC_INSTPATH);

    if (hwndCtl)
    {
        SetWindowText(hwndCtl, g_szAppPath);
    }
}

BOOL GetAppPath(HWND hwndDlg)
{
    HWND hwndCtl = GetDlgItem(hwndDlg, IDC_INSTPATH);

    if (hwndCtl && GetWindowText(hwndCtl, g_szAppPath, MAX_PATH))
        return TRUE;

    SetAppPath(hwndDlg);
    return FALSE;
}

void InitAppPath(HWND hwndDlg)
{
    if (SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, g_szAppPath) != S_OK)
        sprintf(g_szAppPath, "C:\\Program Files");

    sprintf(g_szAppPath, "%s\\Acclaim Entertainment\\Re-Volt", g_szAppPath);

    GetRegistryPath(REG_INSTALL_KEY);
    GetRegistryPath(REG_DIRECTPLAY_KEY);

    SendDlgItemMessage(hwndDlg, IDC_INSTPATH, EM_SETLIMITTEXT, (WPARAM)MAX_PATH, 0);
    SetAppPath(hwndDlg);

    GetCurrentDirectory(MAX_PATH, g_szCurrentDir);
}

void ResetWorkingDir(void)
{
    SetCurrentDirectory(g_szCurrentDir);
}

BOOL SetWorkingDir(HWND hwndDlg)
{
    GetAppPath(hwndDlg);

    if (!SetCurrentDirectory(g_szAppPath))
    {
        MessageBox(hwndDlg, "Invalid path!", "Error", MB_OK);
        return FALSE;
    }

    WIN32_FIND_DATA fd;
    BOOL bFailure = FALSE;

    HANDLE handle = FindFirstFile(".\\levels\\frontend\\frontend.inf", &fd);
    if (handle == INVALID_HANDLE_VALUE)
        bFailure = TRUE;

    FindClose(handle);

    if (bFailure && MessageBox(hwndDlg, 
        "Could not find Re-Volt game data!\r\n"
        "Are you sure you want to continue?", "Warning", 
        MB_OKCANCEL | MB_DEFBUTTON2 | MB_ICONWARNING) != IDOK)
    {
        ResetWorkingDir();
        return FALSE;
    }

    return TRUE;
}

void DisableControls(HWND hwndDlg)
{
    DeleteMenu(GetSystemMenu(hwndDlg, FALSE), 0xf060, MF_BYCOMMAND);

    EnableWindow(GetDlgItem(hwndDlg, IDOK), FALSE);
    EnableWindow(GetDlgItem(hwndDlg, IDCANCEL), FALSE);
    EnableWindow(GetDlgItem(hwndDlg, IDC_BROWSE), FALSE);
    EnableWindow(GetDlgItem(hwndDlg, IDC_SHORTCUT), FALSE);
    SendDlgItemMessage(hwndDlg, IDC_INSTPATH, EM_SETREADONLY, (WPARAM)TRUE, 0);

    WaitForMessages(hwndDlg, 10);
}

void SetDlgIcon(HWND hwndDlg)
{
    HICON hIcon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(IDI_ICON1));

    if (hIcon)
    {
        SendMessage(hwndDlg, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
        SendMessage(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
    }
}

void SetDlgCtrlFont(HWND hwndDlg, int idCtl, int nSize, int fwWeight, bool bItalic, const char *pszName)
{
    HWND hwndCtl = GetDlgItem(hwndDlg, idCtl);

    if (hwndCtl)
    {
        HDC hdc = GetDC(hwndCtl);
        int height = -MulDiv(nSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);

        HFONT hFont = CreateFont(height, 0, 0, 0,
            fwWeight, bItalic, FALSE, FALSE, ANSI_CHARSET,
            OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
            DEFAULT_PITCH | FF_DONTCARE, pszName);

        SendMessage(hwndCtl, WM_SETFONT, (WPARAM)hFont, 0);
    }
}

void LoadDlgCtrlFont(void)
{
    HRSRC res = FindResource(NULL, MAKEINTRESOURCE(IDF_CONSOLE), RT_FONT);
    if (res)
    {
        HGLOBAL mem = LoadResource(GetModuleHandle(0), res);
        void *data = LockResource(mem);
        size_t len = SizeofResource(GetModuleHandle(0), res);

        DWORD nFonts;
        g_hFontHandle = AddFontMemResourceEx(
                data, // font resource
                len, // number of bytes in font resource
                NULL, // Reserved. Must be 0.
                &nFonts // number of fonts installed
                );
    }
}

int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lp, LPARAM data)
{
    switch(uMsg)
    {
    case BFFM_INITIALIZED:

        SendMessage(hwnd, BFFM_SETSELECTION, TRUE, data);
        break;

    case BFFM_SELCHANGED:

        //char dir[MAX_PATH];
        //if (SHGetPathFromIDList((LPITEMIDLIST)lp, dir))
        //    SendMessage(hwnd, BFFM_SETSTATUSTEXT, 0, (LPARAM)dir);
        break;
    }

    return 0;
}

void BrowseForFolder(HWND hwndDlg)
{
    BROWSEINFO browseInfo;
    LPITEMIDLIST itemIDList;

    ZeroMemory(&browseInfo, sizeof(browseInfo));
    browseInfo.hwndOwner = hwndDlg;
    browseInfo.lpszTitle = "Select the folder where Re-Volt is installed.";
    browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;// | BIF_STATUSTEXT;
    browseInfo.lpfn = BrowseCallbackProc;
    browseInfo.lParam = (LPARAM)g_szAppPath;

    itemIDList = SHBrowseForFolder(&browseInfo);
    if (itemIDList && SHGetPathFromIDList(itemIDList, g_szAppPath))
    {
        SetAppPath(hwndDlg);
        SendMessage(hwndDlg, DM_SETDEFID, (WPARAM)IDOK, 0);
        SendDlgItemMessage(hwndDlg, IDC_BROWSE, BM_SETSTYLE, (WPARAM)BS_PUSHBUTTON, (LPARAM)TRUE);
    }
}

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:

        SetDlgIcon(hwndDlg);

        LoadDlgCtrlFont();
        SetDlgCtrlFont(hwndDlg, IDC_INSTHEAD, 12, FW_HEAVY, FALSE, "Liberation Sans");
        SetDlgCtrlFont(hwndDlg, IDC_INSTBLOCK, 10, FW_NORMAL, FALSE, "Liberation Sans");
        SetDlgCtrlFont(hwndDlg, IDC_INSTFOOT, 8, FW_LIGHT, TRUE, "Liberation Sans");

        CheckDlgButton(hwndDlg, IDC_SHORTCUT, BST_CHECKED);

        InitAppPath(hwndDlg);
        return TRUE;

    case WM_CLOSE:

        if (!g_bInstalling)
            EndDialog(hwndDlg, 0);
        return TRUE;

    case WM_COMMAND:

        if (g_bInstalling)
            return TRUE;

        switch(LOWORD(wParam))
        {
        case IDC_BROWSE:

            BrowseForFolder(hwndDlg);
            break;

        case IDCANCEL:

            EndDialog(hwndDlg, 0);
            break;

        case IDOK:

            if (SetWorkingDir(hwndDlg))
            {
                g_bInstalling = TRUE;
                DisableControls(hwndDlg);

                int res = DoInstall(hwndDlg);
                FinalizeInstall(hwndDlg, res);

                ResetWorkingDir();
                g_bInstalling = FALSE;
                EndDialog(hwndDlg, res);
            }
            break;
        }

        return TRUE;
    }
    return FALSE;
}

BOOL InitializeApp(void)
{
    BOOL isAdmin;
    PSID AdminGroup;
    DWORD dwSize = SECURITY_MAX_SID_SIZE;

// check if user has admin privileges

    if (!(AdminGroup = malloc(dwSize)) ||

        !CreateWellKnownSid(WinBuiltinAdministratorsSid,
            NULL,
            AdminGroup,
            &dwSize) ||

        !CheckTokenMembership( NULL, AdminGroup, &isAdmin))
    {
        isAdmin = FALSE;
    }

    if (AdminGroup) free(AdminGroup);

    if (!isAdmin)
    {
#ifndef _DEBUG
        MessageBox(NULL, "Setup needs to be run in Administrator mode.", "Error", MB_OK);
        return FALSE;
#endif
    }

// init common controls

    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    InitCtrls.dwICC = ICC_STANDARD_CLASSES | ICC_PROGRESS_CLASS;

    if (!InitCommonControlsEx(&InitCtrls))
    {
        MessageBox(NULL, "Can't init common controls.", "Error", MB_OK);
        return FALSE;
    }

    return TRUE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    if (!InitializeApp())
        return FALSE;

    return DialogBox(hInstance, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
