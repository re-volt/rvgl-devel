#!/bin/bash

for i in "$@"; do
  case $i in
    --debug) debug=true;;
    --clean) clean=true;;
  esac
done

if [ "$clean" = true ]; then
  rm -rf ./libs
  rm -rf ./obj
  rm -rf ./bin
  rm -rf ./gen
  exit
fi

if [ "$debug" = true ]; then
  flags="-j $(nproc) DEBUG=1"
else
  flags="-j $(nproc)"
fi

ndk-build $flags
ant debug

if [ ! "$debug" = true ]; then
  cp bin/com.rvgl.controllermap-debug.apk ../../../distrib/rvgl_controllermap.apk
fi
