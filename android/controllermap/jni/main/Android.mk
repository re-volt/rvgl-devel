LOCAL_PATH := $(call my-dir)
SDL_PATH := ../SDL2

include $(CLEAR_VARS)

LOCAL_MODULE := main

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include

LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
                   $(subst $(LOCAL_PATH)/,, \
                   $(LOCAL_PATH)/controllermap.c \
                   $(LOCAL_PATH)/debug.c)

LOCAL_SHARED_LIBRARIES := SDL2

LOCAL_CFLAGS += -std=gnu11

LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)
