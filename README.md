# RVGL Development

Part of the [RVGL Base Repository](https://gitlab.com/re-volt/rvgl-base). 
Do not clone this repository directly. The base repository includes this 
as a sub-module.

This repository consists of development headers and libraries intended for 
building RVGL and its supporting tools.

In addition, this repository contains the following sub-projects:

- Android Controller Map (`android/controllermap`)
- DirectPlay Lobby Helper (`windows/dplobby`)
- Windows Installer (`windows/setup`)

